# Executes the pre-parsed content.
# Author: cursefroge
# Created: 6/3/2024
import sys

from parser import Token


def postfix(tokens):
    precedence = {Token.PLUS: 1, Token.MINUS: 1, Token.MUL: 2, Token.DIV: 2}
    output = []
    operator_stack = []

    for token, data in tokens:
        if token == Token.NUMBER or token == Token.MAX_INT or token == Token.SIGNED_NUMBER:
            output.append((token, data))
        elif token in (Token.PLUS, Token.MINUS, Token.MUL, Token.DIV):
            while operator_stack and operator_stack[-1][0] != Token.LPAREN and precedence[operator_stack[-1][0]] >= \
                    precedence[token]:
                output.append(operator_stack.pop())
            operator_stack.append((token, data))
        elif token == Token.LPAREN:
            operator_stack.append((token, data))
        elif token == Token.RPAREN:
            while operator_stack and operator_stack[-1][0] != Token.LPAREN:
                output.append(operator_stack.pop())
            operator_stack.pop()  # Discard the left parenthesis

    while operator_stack:
        output.append(operator_stack.pop())

    return output


def execute(tokens):
    tokens = postfix(tokens)  # Convert tokens to postfix notation
    stack = []
    for token, data in tokens:
        if token == Token.NUMBER or token == Token.SIGNED_NUMBER:
            stack.append(data['value'])
        elif token == Token.PLUS:
            stack.append(stack.pop() + stack.pop())
        elif token == Token.MINUS:
            second = stack.pop()
            first = stack.pop()
            stack.append(first - second)
        elif token == Token.MUL:
            stack.append(stack.pop() * stack.pop())
        elif token == Token.DIV:
            second = stack.pop()
            first = stack.pop()
            stack.append(first / second)
        elif token == Token.MAX_INT:
            stack.append(sys.maxsize * 2 + 1)
        elif token == Token.EOF:
            break
    # if the thing we're returning can be an integer, return an integer
    if stack[0] % 1 == 0:
        return int(stack.pop())
    return stack.pop()
