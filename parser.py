# Parser for the Berry language
# Author: cursefroge
# Created: 6/3/2024
# Parser for the Berry language
# Author: cursefroge
# Created: 6/3/2024
from util import Token
import re
from decimal import Decimal


def tokenize(content):
    tokens = []
    content_tokens = re.findall(r'-?\d+\.\d+|\b\d+\b|MAX_INT|\w+|\S', content)  # Use regex to split content into tokens
    for i, token in enumerate(content_tokens):
        if token.startswith('-') and token[1:].replace('.', '', 1).isdigit():
            tokens.append((Token.SIGNED_NUMBER, {'value': -Decimal(token[1:])}))
        elif token.replace('.', '', 1).isdigit():
            tokens.append((Token.NUMBER, {'value': Decimal(token)}))
        elif token == '+':
            tokens.append((Token.PLUS, {}))
        elif token == '-':
            tokens.append((Token.MINUS, {}))
        elif token == '*':
            tokens.append((Token.MUL, {}))
        elif token == '/':
            tokens.append((Token.DIV, {}))
        elif token == '(':
            tokens.append((Token.LPAREN, {}))
        elif token == ')':
            tokens.append((Token.RPAREN, {}))
        elif token == 'MAX_INT':
            tokens.append((Token.MAX_INT, {}))
        elif token.isalpha():
            print(token)
            tokens.append((Token.WORD, {'value': token}))
    tokens.append((Token.EOF, {}))
    return tokens
