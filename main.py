# parse and execute 1+2*3
from execute import execute
from parser import tokenize

with open("test.bry") as f:
    stuff = tokenize(f.read())
print(stuff)
print(execute(stuff))
