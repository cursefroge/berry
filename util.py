# Utilites and core types for Berry
# Author: cursefroge
# Created: 6/3/2024
from enum import Enum
import pickle


class Token(Enum):
    EOF = 0
    NUMBER = 1
    PLUS = 2
    MINUS = 3
    MUL = 4
    DIV = 5
    LPAREN = 6
    RPAREN = 7
    MAX_INT = 8
    SIGNED_NUMBER = 9
    WORD = 10


def save(tokens, filename):
    # Convert Token enum to its corresponding value
    tokens_value = [(token.value, data) for token, data in tokens]
    with open(filename, 'wb') as f:
        pickle.dump(tokens_value, f)


def load(filename):
    with open(filename, 'rb') as f:
        # Convert value back to Token enum
        tokens_value = pickle.load(f)
        return [(Token(token), data) for token, data in tokens_value]
